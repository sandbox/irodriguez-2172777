/**
 * Javascript for the alert_message module.
 */

(function($){
  Drupal.behaviors.alert_message = {
    attach: function(context) {
      var el = $("blink");
      setInterval(function() {(el.css("visibility") == "hidden")?el.css("visibility","visible"):el.css("visibility","hidden");}, 500);
      $('.arrow-up').click(function() {
        $('.alerts').slideUp(function() { $('.arrow-down').slideDown(); });
      });
      $('.arrow-down').click(function() {
        $('.arrow-down').slideUp(function() { $('.alerts').slideDown(); });
      });
    }
  };
})(jQuery);
