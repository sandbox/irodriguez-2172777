Generates an alert message to be displayed at the top of web page the user is
viewing so that site administrators can communicate important notifications to
their user. An example of usage is to display weather alerts to users during
snow storms. Another usage would be to communicate scheduled site down time.
Administrators can add, edit, and delete custom colors and (background color, 
border color, font color) for messages.
